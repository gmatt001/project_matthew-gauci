<?php
/* Template Name: Database_display */


get_header();

   

    if (isset($_POST))
    {
        global $wpdb;     
        

        $database = 'feedback_database';

        

            
            $name = $_POST['inputname'];
            $surname = $_POST['inputsurname'];
            $email = $_POST['inputemail'];
            $feedback = $_POST['inputfeedback'];
        
        
        $data = array(
            'name'=>$name,
            'surname'=>$surname,
            'email'=>$email,
            'feedback'=>$feedback,
        );
                

        
        
        
  
        print_r($data);
        $wpdb->insert($database, $data); 
    }

?>


<div class="blog-content-wrapper">
    <div class="container blog-content">
        <div class="row">
            
            <div class="col-md-8">
                
                    
                    <?php 
                    
                        while ( have_posts() ) : the_post(); ?>
                                <?php get_template_part( 'template-parts/content', 'single' ); ?>
                                <?php the_post_navigation(); ?>
                                <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;
                                

                            endwhile;  ?>
    
                            
                
                            <main class="container">
                                <form action="#" method="post">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="inputname" placeholder="Please enter your name" class="form-control" required/>
                                </div>
                                <div class="form-group">
                                    <label>Surname</label>
                                    <input type="text" name="inputsurname" placeholder="Please enter surname" class="form-control" required/>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="inputemail" placeholder="Please enter email" class="form-control" required/>
                                </div>
                                 <div class="form-group">
                                    <label>Feedback</label>
                                    <input type="text" name="inputfeedback" placeholder="Please enter your feedback" class="form-control" />
                                </div>
                                <div class="form-group pull-right">
                                <input class="btn btn-primary btn-sm" type="submit" value="send us feedback"/>
                                <input id="button" class="btn btn-sm btn-primary" type="reset" value="clear values!"/>
                                </div>
                                </form>

                                </main>

                                <style>
                                .container{width:80%;}
                                .body{background-color:white;}
                                </style>
                     
                            
                             
                       
            </div>
        </div>
    </div>
    </div>


    <?php get_footer(); ?>
